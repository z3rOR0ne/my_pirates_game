const http = require('http')
const express = require('express')
const app = require('express')()
const websocketServer = require('websocket').server
const httpServer = http.createServer()
const { nanoid } = require('nanoid')

// express serves index.html
app.listen('9091', () => console.log('Listening on http port 9091'))

// serve css and js in public folder
app.use(express.static('public'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

httpServer.listen(9090, () => console.log('Listening.. on 9090'))

// hashmap
const clients = {}
const games = {}
const wsServer = new websocketServer({
    httpServer: httpServer,
})

// random number generator to be used for positioning players within html canvas
function randNum(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}

const xArr = []
const yArr = []

wsServer.on('request', request => {
    // connect
    const connection = request.accept(null, request.origin)
    connection.on('message', message => {
        const result = JSON.parse(message.utf8Data)

        // create
        if (result.method === 'create') {
            console.log(
                'index.js has received create event after new game button is pushed',
            )

            console.log(result)
            const clientId = result.clientId
            const gameId = nanoid()
            console.log('game successfully created with id ' + gameId)

            games[gameId] = {
                id: gameId,
                clients: [],
            }

            const payLoad = {
                method: 'create',
                game: games[gameId],
            }

            const con = clients[clientId].connection
            con.send(JSON.stringify(payLoad))
        }
        // join
        if (result.method === 'join') {
            // gets the clientId passed from the original create event
            const clientId = result.clientId

            // gets the game id from the html form
            const gameId = result.gameId
            const game = games[gameId]

            const color = {
                0: 'Red',
                1: 'Green',
                2: 'Blue',
            }

            const name = {
                0: 'Blackbeard',
                1: 'Jimmy',
                2: 'Roger',
            }

            game.clients.push({
                // replace clientId with names object
                clientId: clientId,
                name: name[game.clients.length],
                color: color[game.clients.length],
                xArr: xArr,
                yArr: yArr,
            })

            // immediately upon game creation establish three random vertices to put avatars
            for (let i = 0; i < game.clients.length; i++) {
                xArr.push(randNum(0, 500))
                yArr.push(randNum(0, 500))
            }

            const payload = {
                method: 'join',
                game: game,
            }

            for (let i = 0; i < game.clients.length; i++) {
                let c = game.clients[i]
                clients[c.clientId].connection.send(JSON.stringify(payload))
            }
        }
    })

    // generate a new clientId upon 'connect' event
    const clientId = nanoid()

    clients[clientId] = {
        connection: connection,
    }

    const payload = {
        method: 'connect',
        clientId: clientId,
    }
    // send back to the front end the 'connection' event that ws will listen for
    connection.send(JSON.stringify(payload))
})

## A converting over of Pirates Ship Fork

[Original Project by sgoedecke](https://github.com/sgoedecke/socket-io-game)

This is a simple fork of a project creating a basic Socket IO pirate game by sgoedecke on github. In order to better understand the basic Web Sockets Protocol, this project is simply converting the original fork's code over from utilizing Socket IO to Web Sockets.

let clientId = null
let gameId = null
let ws = new WebSocket('ws://localhost:9090')
const btnCreate = document.getElementById('btnCreate')
const btnJoin = document.getElementById('btnJoin')
const txtGameId = document.getElementById('txtGameId')
const divPlayers = document.getElementById('divPlayers')
const canv = document.getElementById('canv')

// wiring events
// intializes 'create' event
btnCreate.addEventListener('click', e => {
    console.log('new game button clicked')
    const payLoad = {
        method: 'create',
        clientId: clientId,
    }
    ws.send(JSON.stringify(payLoad))
})

// initializes 'join' event
btnJoin.addEventListener('click', e => {
    // grab the value of the input field
    gameId = txtGameId.value

    console.log('join button clicked')
    const payLoad = {
        method: 'join',
        clientId: clientId,
        gameId: gameId,
    }
    ws.send(JSON.stringify(payLoad))
})

ws.onmessage = message => {
    const response = JSON.parse(message.data)
    // connect
    if (response.method === 'connect') {
        clientId = response.clientId
        console.log('Client Id Set Successfully ' + clientId)

        const canvas = document.createElement('canvas')
        canvas.id = 'game'
        canvas.width = 500
        canvas.height = 500
        canv.appendChild(canvas)
    }
    // create
    if (response.method === 'create') {
        console.log(response)
        console.log('game successfully created with id ' + response.game.id)
    }

    // join
    if (response.method === 'join') {
        const game = response.game
        console.log('game object:')
        console.log(game)

        while (divPlayers.firstChild) {
            divPlayers.removeChild(divPlayers.firstChild)
        }

        const canvas = document.getElementById('game')
        const ctx = canvas.getContext('2d')
        ctx.clearRect(0, 0, canvas.width, canvas.height)

        game.clients.forEach((c, i) => {
            // Found success here, originally we passed just the individual x and y coordinates.
            // Instead, here we simply reference the actual array passed from index.js and we
            // can draw on html canvas in real time

            ctx.fillStyle = c.color
            ctx.fillRect(c.xArr[i], c.yArr[i], 10, 10)
            // }
        })

        game.clients.forEach(c => {
            // draws the clients names
            const d = document.createElement('div')

            d.style.width = '200px'
            d.style.background = c.color
            d.textContent = c.name
            divPlayers.appendChild(d)
        })
    }
}
